#include <iostream>
#include <string>



void error(const char* name, const char* msg) {
    std::cerr << name << ": " << msg << std::endl;
    exit(1);
}

int main(int argc, char* argv[]) {
    if (argc < 3)
        error(argv[0], "should have at least 2 integers as parameters (a,b) strange_op (a+b, a*b)");
    
    try {
        int a = std::stoi(argv[1]);
        int b = std::stoi(argv[2]);

        int r1 = (a+b)%16;
        int r2 = (a*b)%16;

        std::cout << r1 << " " << r2 << std::endl;
    }
    catch (std::logic_error &e) {
        error(argv[0], "Parsing arguments");
    }

    return 0;
}