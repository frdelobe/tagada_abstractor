use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct SOperator { 
    base: BaseOperator,
    sbox: Vec<Value>,
}

impl SOperator {
    pub fn alone(i_size: Value, i_number: usize,        
        sbox: Vec<Value> ) -> Self {            
            Self {                 
                base: BaseOperator::alone(i_size, i_number, i_size, i_number),
                sbox: sbox    
            } 
    }
}

use crate::json_save::{flatten_arguments_1d};

impl Operator for SOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("S")
    }
    fn parameters2s(&self) -> String     
    {   
        format!(".p.{}",flatten_arguments_1d(&self.sbox).as_str() )
    }
}


impl Computable for SOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        input.iter().map(|i| self.sbox[*i as usize] ).collect()
    }
}

#[cfg(test)]
mod tests {
        
    use super::*;
    #[test]
    fn it_computes_operator() {
        let sbox = SOperator::alone(4 ,4 , vec![3,2,1,0] );                

        assert_eq!(sbox.compute( &vec![0,1,2,3] ),vec![3,2,1,0] );        
        assert_eq!(sbox.compute( &vec![3,2,1,0] ),vec![0,1,2,3] );        
        assert_eq!(sbox.compute( &vec![0,0,0,0] ),vec![3,3,3,3] );
    }
    use crate::base::SolutionSet;
    
    #[test]
    pub fn it_enumerate_operator() {
        let sbox = SOperator::alone(3 ,1 , vec![0,1,2,3] );                        
        let to_bool = crate::base::BooleanAbstraction { };

        let result : SolutionSet = [ 
                 (vec![1, 1], 3), 
                 (vec![0, 0], 1),                 
        ].iter().cloned().collect::<SolutionSet>();

        assert_eq! (
            crate::base::enumerate(&sbox, &to_bool), 
            result
        );        
    }

}