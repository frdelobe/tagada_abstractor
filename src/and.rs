use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct AndOperator { 
    base: BaseOperator,
}

impl AndOperator {
    pub fn alone(i_size: Value, i_number: usize,
                 o_size: Value, o_number: usize ) -> Self {
            Self { base: BaseOperator::alone(i_size, i_number, o_size, o_number) } 
    }
}

impl Operator for AndOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }   

    fn name(&self) -> String {
        String::from("And")
    }    
}


impl Computable for AndOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        let mut accu = input[0];
        for i in input {
            accu &= i;
        }

        vec! [ accu ]
    }
}

#[cfg(test)]
mod tests {
        
    //use crate::base::{BaseOperator, Operator, Computable, Value};
    #[test]
    fn it_has_a_proper_name() {
        let and = AndOperator::alone(255 ,2 ,255 ,1 );                
        assert_eq!(and.name(), "And"); 
    }

    #[test]
    fn it_has_a_proper_uid() {
        let and = AndOperator::alone(255 ,2 ,255 ,1 );  
        println!("==> {:?}", and.uid() );              
        assert_eq!(and.uid(), "And.i.0_255.0_255.o.0_255"); 
    }

    use super::*;
    #[test]
    fn it_computes_operator() {
        let and = AndOperator::alone(255 ,2 ,255 ,1 );                

        assert_eq!(and.compute( &vec![0,0] ),vec![0] );
        assert_eq!(and.compute( &vec![0,1] ),vec![0] );
        assert_eq!(and.compute( &vec![1,0] ),vec![0] );
        assert_eq!(and.compute( &vec![1,1] ),vec![1] );

        assert_eq!(and.compute( &vec![1,1,1] ),vec![1] );
        assert_eq!(and.compute( &vec![1,1,0] ),vec![0] );
        assert_eq!(and.compute( &vec![1,0,0] ),vec![0] );
    }

    use crate::base::SolutionSet;

    #[test]
    pub fn it_enumerate_operator() {
        let and = AndOperator::alone(15 ,2 ,16 ,1 );
        let to_bool = crate::base::BooleanAbstraction { };
        let result : SolutionSet = [ 
                 (vec![0, 1, 0],  15), 
                 (vec![1, 0, 0],  15), 
                 (vec![0, 0, 0],   1),                 
                 (vec![1, 1, 0],  50), 
                 (vec![1, 1, 1], 175),
        ].iter().cloned().collect::<SolutionSet>();

        assert_eq! (
            crate::base::enumerate(&and, &to_bool), 
            result
        );          
    }

}