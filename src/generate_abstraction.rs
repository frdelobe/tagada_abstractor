use serde_json::Value;
use crate::base;
use crate::base::{Operator, Domain};
use crate::xor::XorOperator;
use crate::sbox::SOperator;

pub fn enumerate_and_export<T:Operator>(_t: T) {    
}

/**
*   name: {"inputs", "outputs"}
*/
pub fn extract_domain_vec(json: &Value, name:&str) -> Vec<Domain> {
    match json { // Object type
        Value::Object(m) => {
            match &m[name] {
                Value::Array(a) => {
                    a.iter().map(|e| { 
                        match e {
                            Value::Object(r) => {
                                match &r["type"] {
                                    Value::String(s) => {
                                        if s == "range" {
                                            r["min"].as_u64().unwrap() as u32 .. r["max"].as_u64().unwrap() as u32
                                        } else {
                                            panic!("computeOperatorFromJson: unknown range type");
                                        }
                                    },
                                    _ => { panic!("computeOperatorFromJson: 'type' should be a string"); }
                                }
                            },
                            _ => { panic!("computeOperatorFromJson: I was expecting an object (domain)"); }
                        }
                    }).collect::<Vec<Domain>>()
                },
                _ => { panic!("computeOperatorFromJson: I was expecting an array"); }
            }
        }
        _ => { panic!("computeOperatorFromJson: invalid json (should be of type Object)"); }
    }
}


/**
 call enumerateAndExport(t) : t being the operator defined in "json"
*/
pub fn compute_operator_from_json(json: &Value) {
    let inputs = extract_domain_vec(json, "inputs");
    let outputs = extract_domain_vec(json, "outputs");
    match json { // Object type
        Value::Object(m) => {
            match m["name"].as_str().unwrap() {
                "xor" => {
                    enumerate_and_export(XorOperator::alone(
                        inputs[0].end, inputs.len(),
                        outputs[0].end, outputs.len()
                    ));
                },
                "s" => {
                    let table:Vec<base::Value> = m["params"].as_object().unwrap()
                        ["table"].as_array().unwrap().iter().map({|e|
                            e.as_u64().unwrap() as base::Value
                        }).collect();
                    enumerate_and_export(SOperator::alone(
                        inputs[0].end, 1, table 
                    ));
                },
                _ => { panic!("unknown operator name"); }
            }
        },
        _ => { panic!("computeOperatorFromJson: invalid json (should be of type Object)"); }
    }
}