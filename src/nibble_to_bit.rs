use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct NibbleToBitsOperator { 
    base: BaseOperator,
    cache: Vec<Vec<Value>>
}

impl NibbleToBitsOperator {
    pub fn alone() -> Self {
        let mut cache: Vec<Vec<Value>>=Vec::new();

        let bits = 4;

        for i in 0..16 {
            let mut result: Vec<Value>=Vec::new();
            let mut x = i;
            for _ in 0..bits {
                result.push(x % 2);
                x /= 2;
            }
            result.reverse();
            cache.push(result);
        }
        Self { 
            base: BaseOperator::alone(15, 1, 1, 4 ),
            cache: cache
        } 
    }
}

impl Operator for NibbleToBitsOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("NibbleToBits")
    }
}

impl Computable for NibbleToBitsOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        self.cache[input[0] as usize].clone() 
    }        
}
    

#[cfg(test)]
mod tests {
        
    use super::*;

    #[test]
    fn it_computes_operator() {
        let n2b = NibbleToBitsOperator::alone();                
    
        assert_eq!(n2b.compute( &vec![ 0 ] ),vec![0, 0, 0, 0] );        
        assert_eq!(n2b.compute( &vec![ 2 ] ),vec![0, 0, 1, 0] );        
        assert_eq!(n2b.compute( &vec![ 7 ] ),vec![0, 1, 1, 1] );        
        assert_eq!(n2b.compute( &vec![15 ] ),vec![1, 1, 1, 1] );        
    }

    use crate::base::SolutionSet;
    #[test]
    pub fn it_enumerate_operator() {
        let n2b = NibbleToBitsOperator::alone();
        let to_bool = crate::base::BooleanAbstraction { };

        let result : SolutionSet = [ 
                 (vec![1, 0, 0, 1, 1], 1), 
                 (vec![1, 1, 0, 1, 0], 1), 
                 (vec![1, 1, 0, 0, 0], 1), 
                 (vec![1, 1, 0, 1, 1], 1), 
                 (vec![1, 1, 1, 0, 0], 1), 
                 (vec![1, 0, 1, 0, 0], 1), 
                 (vec![1, 0, 1, 1, 0], 1), 
                 (vec![1, 1, 1, 0, 1], 1), 
                 (vec![1, 0, 1, 1, 1], 1), 
                 (vec![1, 1, 1, 1, 0], 1), 
                 (vec![1, 1, 0, 0, 1], 1), 
                 (vec![1, 0, 0, 1, 0], 1), 
                 (vec![0, 0, 0, 0, 0], 1), 
                 (vec![1, 0, 0, 0, 1], 1), 
                 (vec![1, 1, 1, 1, 1], 1), 
                 (vec![1, 0, 1, 0, 1], 1),
        ].iter().cloned().collect::<SolutionSet>();
 
        assert_eq! (
            crate::base::enumerate(&n2b, &to_bool), 
            result
        );          
    }

}