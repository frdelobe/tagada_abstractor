
use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct LfsrOperator { 
    base: BaseOperator,
    matrix: Vec<Vec<Value>>,
}

impl LfsrOperator {
    pub fn alone(i_size: Value, matrix: Vec<Vec<Value>> ) -> Self {            
            Self { 
                base: BaseOperator::alone(i_size, 1, i_size, 1),
                matrix: matrix,
            }
    }
}

use crate::json_save::flatten_arguments;

impl Operator for LfsrOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("LFSR")
    }
    fn parameters2s(&self) -> String {        
        format!(".p.{}", flatten_arguments(&self.matrix).as_str() )
    }
}

use crate::base::nb_necessary_bits;

impl Computable for LfsrOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {        
        let nbits = nb_necessary_bits( (self.base.inputs()[0].end+1)  as usize); 

        let input_bits = (0..nbits).into_iter()
                .map(|i| (((input[0] & (1<<i)) ) >> i)  ).rev().collect::<Vec<u32>>();
        
        let output_bits = (0..nbits).into_iter().map(|i| {
            let mut res = 0;

            for j in &self.matrix[i as usize] {
                res ^= input_bits[*j as usize];                
            }
            res
        }
        ).collect::<Vec<u32>>();

        let mut out_value = 0;
        let mut c = 1;
        for e in output_bits.iter().rev() {
            out_value += e*c;
            c*=2;
        }
        
        vec![out_value as Value]

    }
}


#[cfg(test)]
mod tests {
            
    use crate::base::SolutionSet;    

    use super::*;

    #[test]
    fn it_has_a_proper_uid() {
        let lfsr = LfsrOperator::alone( 255 ,
            vec![
                vec![1],
                vec![2],
                vec![3],
                vec![4],
                vec![5],
                vec![6],            
                vec![7],
                vec![0,1]
            ]            
        );      
        assert_eq!(lfsr.uid(), "LFSR.p.1+2+3+4+5+6+7+0_1.i.0_255.o.0_255");
    }

    #[test]
    fn it_computes_operator_for_nibbles() {
        let lsfr = LfsrOperator::alone( 15 ,
            vec![
                vec![1],
                vec![2],
                vec![3],
                vec![0,1]
            ]            
        );
        assert_eq!(lsfr.compute(&vec![0] ),  [0]);
        assert_eq!(lsfr.compute(&vec![1] ),  [2]);
        assert_eq!(lsfr.compute(&vec![8] ),  [1]);
        assert_eq!(lsfr.compute(&vec![12]),  [8]);
        assert_eq!(lsfr.compute(&vec![7] ), [15]);                         
    }

    #[test]
    fn it_computes_operator_for_bytes() {
        let lsfr = LfsrOperator::alone( 255 ,
            vec![
                vec![1],
                vec![2],
                vec![3],
                vec![4],
                vec![5],
                vec![6],            
                vec![7],
                vec![0,1]
            ]            
        );
        assert_eq!(lsfr.compute(&vec![  0] ),  [0]);
        assert_eq!(lsfr.compute(&vec![  1] ),  [2]);
        assert_eq!(lsfr.compute(&vec![128] ),  [1]);
    }

    #[test]
    pub fn it_enumerate_operator_for_nibbles() {
        let lfsr = LfsrOperator::alone( 255 ,
            vec![
                vec![1],
                vec![2],
                vec![3],
                vec![4],
                vec![5],
                vec![6],            
                vec![7],
                vec![0,1]
            ]            
        );
        let to_bool = crate::base::BooleanAbstraction { };

        let result : SolutionSet = [ 
                (vec![0, 0],     1),            
                (vec![1, 1],   255),
        ].iter().cloned().collect::<SolutionSet>();

        assert_eq! (
            crate::base::enumerate(&lfsr, &to_bool), 
            result
        );        
    }

}