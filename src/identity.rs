use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct IdentityOperator { 
    base: BaseOperator,
}

impl IdentityOperator {
    pub fn alone(i_size: Value, i_number: usize ) -> Self {
            Self { base: BaseOperator::alone(i_size, i_number, i_size, i_number) } 
    }
}

impl Operator for IdentityOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("Equality")
    }
}

impl Computable for IdentityOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        input.clone()
    }
}
    

#[cfg(test)]
mod tests {
        
    use super::*;
    #[test]
    fn it_computes_operator() {
        let identity = IdentityOperator::alone(256 ,2);                

        for i in 0..256 {
            assert_eq!(identity.compute( &vec![i] ),vec![i] );
        }
    }
    use crate::base::SolutionSet;
    #[test]
    pub fn it_enumerate_operator() {
        let identity = IdentityOperator::alone(255 ,1 );
        let to_bool = crate::base::BooleanAbstraction { };
        let result : SolutionSet = [ 
                 (vec![1, 1], 255), 
                 (vec![0, 0], 1),                 
        ].iter().cloned().collect::<SolutionSet>();

        assert_eq! (
            crate::base::enumerate(&identity, &to_bool), 
            result
        );          
    }

}