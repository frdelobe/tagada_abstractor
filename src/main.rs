mod and; // Si pas inclus, pas de tests
mod base;
mod bit_to_nibble;
mod byte_to_nibble;
mod identity;
mod json_save;
mod lfsr;
mod mixcolumn;
mod nibble_to_bit;
mod sbox;
mod xor;
mod json_loader;

#[macro_use]
extern crate clap;

use crate::json_save::compute_and_save;
use crate::json_loader::build_operator;

fn generate_operator(operator: json_loader::JOperator, destination:& String) {    
    let operator = build_operator(operator);
    let to_bool = base::BooleanAbstraction { };

    #[cfg(debug_assertions)]
    println!("Looking for {}", operator.uid().light_yellow());

    compute_and_save(&destination, &*operator, &to_bool);
}

use std::fs;
use std::path::Path;
use regex::Regex;
use std::io::Write;

fn rebuild_index(dirpath: &str) {
    let path = Path::new(dirpath);
    let pattern = Regex::new(r".*\.abstracted_oper\.json$").unwrap();
    let indexpath = format!("{}/abstracted_oper.index.json", dirpath);

    let mut first = true;
    let mut output = fs::File::create(&indexpath).unwrap();
    output.write("[\n".as_bytes()).expect("Writing in index");    

    for entry in fs::read_dir(path).expect(format!("Unable to list in {}", dirpath).as_str()) {
        let entry = entry.expect("unable to get entry");    
        let entry_name = entry.path(); // Need to own a temporary
        let entry_path = entry_name.to_str().unwrap();

        if pattern.is_match(entry_path ) {
            if ! first {
                output.write(",\n".as_bytes()).expect("Writing in index");
            }
            first = false;

            let content = fs::read(entry_path).unwrap();            
            output.write(&content).expect("Writing index");
        }        
    };
    output.write("\n]".as_bytes()).expect("Writing in index");
    println!("Index generated in {}", indexpath.light_yellow());
}

fn process_dag(path: String, destination: & String) {
    #[cfg(debug_assertions)]
    println!("Processing Dag {}", path.clone().light_yellow());
    let dag = json_loader::parse_dag_file(path);
    
    for operator in dag.operators {
        generate_operator(operator, destination);
    }
    rebuild_index(destination);
}

use clap::{ArgMatches,App};


fn generate(matches: &ArgMatches) {
    let dagfile = matches.value_of("dag").unwrap();
    let destination = matches.value_of("destination").unwrap_or("/tmp");

    process_dag(dagfile.to_string(), &destination.to_string());
}

extern crate colorful;
use colorful::Colorful;

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    match matches.subcommand() {
        ("gen", Some(sub_m)) => {
            generate(sub_m);
            return;
        },
        _ => {},
    }
  println!("{}", "Default args for testing".light_yellow() );

  let tmp = "/tmp".to_string();
  process_dag("midori_128_1.json".to_string(), &tmp);
}



#[cfg(test)]
mod tests {

    use super::*;

    use json_save::compute_and_save;
    use lfsr::LfsrOperator;
    use xor::XorOperator;

    #[test]
    fn it_produce_files() {
        let xor = XorOperator::alone(15, 2, 15, 1);
        let to_bool = base::BooleanAbstraction {};
        compute_and_save(&String::from("/tmp"), &xor, &to_bool);

        let lfsr = LfsrOperator::alone(
            255,
            vec![
                vec![1],
                vec![2],
                vec![3],
                vec![4],
                vec![5],
                vec![6],
                vec![7],
                vec![0, 1],
            ],
        );

        compute_and_save(&String::from("/tmp"), &lfsr, &to_bool);
    }
}
