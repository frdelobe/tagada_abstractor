use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct XorOperator { 
    base: BaseOperator,
}

impl XorOperator {
    pub fn alone(i_size: Value, i_number: usize,
        o_size: Value, o_number: usize ) -> Self {
            Self { base: BaseOperator::alone(i_size, i_number, o_size, o_number) } 
    }
}

impl Operator for XorOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }
    fn name(&self) -> String {
        String::from("Xor")
    }

}


impl Computable for XorOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        let mut accu = 0;
        for i in input {
            accu ^= i;
        }

        vec! [ accu ]
    }
}

#[cfg(test)]
mod tests {
    use crate::base;

    use super::*;

    #[test]
    fn it_has_a_proper_uid() {
        let xor = XorOperator::alone(255 ,2 ,255 ,2 );                
        assert_eq!(xor.uid(), "Xor.i.0_255.0_255.o.0_255.0_255");
    }

    #[test]
    fn it_computes_operator() {
        let xor = XorOperator::alone(256 ,2 ,256 ,2 );                

        assert_eq!(xor.compute( &vec![0,0] ),vec![0] );
        assert_eq!(xor.compute( &vec![0,1] ),vec![1] );
        assert_eq!(xor.compute( &vec![1,0] ),vec![1] );
        assert_eq!(xor.compute( &vec![1,1] ),vec![0] );

        assert_eq!(xor.compute( &vec![1,1,1] ),vec![1] );
        assert_eq!(xor.compute( &vec![1,1,0] ),vec![0] );
        assert_eq!(xor.compute( &vec![1,0,0] ),vec![1] );
    }

    use crate::base::SolutionSet;

    #[test]
    pub fn it_enumerate_operator() {
        let xor = XorOperator::alone(15 ,2 ,15 ,1 );
        let to_bool = base::BooleanAbstraction { };
        let result : SolutionSet = [ 
                 (vec![0, 1, 1], 15), 
                 (vec![0, 0, 0], 1),
                 (vec![1, 1, 0], 15),
                 (vec![1, 0, 1], 15), 
                 (vec![1, 1, 1], 210),
        ].iter().cloned().collect::<SolutionSet>();

        assert_eq! (
            base::enumerate(&xor, &to_bool), 
            result
        );          
    }

}