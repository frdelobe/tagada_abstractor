use serde::{Deserialize, Serialize};

use crate::base::Value;
use crate::base::SolutionSet;

#[derive(Serialize, Deserialize)]
pub struct Solution {
    t: Vec<Value>,
    nb: u64,
}

#[derive(Serialize, Deserialize)]
pub struct Tuples {
    name:   String,
    uid:    String,
    tuples: Vec<Solution>    
}

fn hash2results<Oper: Operator + ?Sized>(oper: & Oper, h: SolutionSet) -> Tuples {
    let mut solutions  = Tuples { 
        name: oper.name(),
        uid:  oper.uid(),
        tuples: Vec::new() 
    };
    for (tuple, occurences) in h {
        solutions.tuples.push(Solution {
            t: tuple.clone(),
            nb: occurences.clone(),
        } 
    )
    }
    solutions
} 

pub fn solutionset2json<Oper: Operator + ?Sized>(oper: &  Oper, h: SolutionSet) -> String {
    let results = hash2results(oper, h);    
    
    // Serialize it to a JSON string.
    let j = serde_json::to_string_pretty(&results);
    match j {
        Ok(s) => { return s; }
        Err(e) => panic!("!!! {} !!!",e)        
    }
}
use crate::base::{Operator, Abstraction, Computable, enumerate};
use std::fs::File;
use std::io::prelude::*;

pub fn compute_and_save<Oper: Operator + Computable + ?Sized>(path:& String, oper: & Oper, abstr: & impl Abstraction) {
    //hash_text( )
    let filename = format!("{}/{}.{}.abstracted_oper.json",path, abstr.uid(), oper.uid_short());
    if std::path::Path::new(&filename).exists() {
        return;
    }

    //#[cfg(debug_assertions)]
    println!("Creating: {}", filename);

    let solution = enumerate(oper,  abstr);
    let json=solutionset2json(oper, solution);

    let mut file = File::create(&filename).expect(&filename);

    file.write_all(json.as_bytes()).expect("Error writing files ");
}


use sha2::{Sha256, Digest};

pub fn hash_text(msg: &str) -> String{
    let mut hasher =  Sha256::default();
    hasher.update(msg);    
    format!("{:x}", hasher.finalize())
}

pub fn flatten_arguments_1d(args: &Vec<Value>) -> String {
    args.iter().map( |i| i.to_string() ).collect::<Vec<String>>().join("_")
}

pub fn flatten_arguments(args: &Vec<Vec<Value>>) -> String {
    args.iter()
        .map(|l1| flatten_arguments_1d(l1)  )
        .collect::<Vec<String>>()
        .join("+")
}


#[cfg(test)]
mod tests {
    use crate::base::SolutionSet;
    use crate::xor::XorOperator;

    use super::*;

    #[test]
    fn it_saves_in_a_string() {
           // Some data structure.
    let s1 = Solution {
        t: vec![0, 0, 0],
        nb: 1,
    };
    let s2 = Solution {
        t: vec![0, 1, 1],
        nb: 15,
    };
    let s3 = Solution {
        t: vec![1, 0, 1],
        nb: 15,
    };
    let s4 = Solution {
        t: vec![1, 1, 1],
        nb: 256 - (1 + 15 + 15 + 15),
    };
    let s5 = Solution {
        t: vec![1, 1, 0],
        nb: 15,
    };

    let mut hm = SolutionSet::default();


    for s in vec![s1, s2, s3, s4, s5] {
        hm.insert(s.t, s.nb);
    }

    let op = XorOperator::alone(255 , 3, 255, 1 );
    let str = solutionset2json(&op, hm);        
    let res = "{\n  \"name\": \"Xor\",\n  \"uid\": \"Xor.i.0_255.0_255.0_255.o.0_255\",\n  \"tuples\": [\n    {\n      \"t\": [\n        0,\n        1,\n        1\n      ],\n      \"nb\": 15\n    },\n    {\n      \"t\": [\n        1,\n        1,\n        1\n      ],\n      \"nb\": 210\n    },\n    {\n      \"t\": [\n        0,\n        0,\n        0\n      ],\n      \"nb\": 1\n    },\n    {\n      \"t\": [\n        1,\n        1,\n        0\n      ],\n      \"nb\": 15\n    },\n    {\n      \"t\": [\n        1,\n        0,\n        1\n      ],\n      \"nb\": 15\n    }\n  ]\n}";
    assert_eq!(str,res);

    }
}