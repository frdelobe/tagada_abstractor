use std::{usize};

use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct MixColumnOperator { 
    base: BaseOperator,
    matrix: Vec<Vec<Value>>,
}

impl MixColumnOperator {
    pub fn alone(i_size: Value, i_number: usize, matrix: Vec<Vec<Value>> ) -> Self {
            
            Self { 
                base: BaseOperator::alone(i_size, i_number, i_size, i_number),
                matrix: matrix,
            }
    }
}

use crate::json_save::flatten_arguments;

impl Operator for MixColumnOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("MixColumns")
    }
    fn parameters2s(&self) -> String {
        format!(".p.{}", flatten_arguments(&self.matrix).as_str() )
    }
}

impl Computable for MixColumnOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {        

        let mut outputs: Vec<Value> = Vec::with_capacity(self.base.inputs().len());
        // @inputs.length.times do |i|
        for i in 0..input.len() {
            let mut tmp = 0;
            for j in 0..input.len() {
                tmp ^= g_mult(input[j], self.matrix[i][j],self.base.inputs()[0].end);
        //   
            }
            outputs.push(tmp);                 
        }        
        outputs
    }
}

fn g_mult(mut a: Value,mut b: Value, domain_size: Value) -> Value {
  let mut p: Value = 0;
  for _counter in 0..8 {
    if  b&1 != 0  {
        p ^= a;
    }

    let hi_bit_set = (a&0x80) != 0;
    a <<= 1;
    if hi_bit_set {
      a ^= 0x1B
    }
    b >>= 1;      
  }  
  (p % domain_size) as Value
}





#[cfg(test)]
mod tests {
        
    use super::*;

    #[test]
    fn it_multiplies_in_galois_fields() {
        // Vérifié avec version Ruby
        assert_eq!(g_mult(12, 12, 255), 80);
        assert_eq!(g_mult(10,  5, 16),   2);
    }

    #[test]
    fn it_computes_operator() {
        let mc = MixColumnOperator::alone(255 ,4,
            vec![ // AES MC
                vec![ 2, 3, 1, 1],
                vec![ 1, 2, 3, 1],
                vec![ 1, 1, 2, 3],
                vec![ 3, 1, 1, 2],
                ]
        );
       
        assert_eq!(mc.compute(&vec![0,0,0,0]),          vec![0,0,0,0]);    
        assert_eq!(mc.compute(&vec![1,1,1,1]),          vec![1,1,1,1]);
        assert_eq!(mc.compute(&vec![219,19,83,69]),     vec![142,77,161,188]);
        assert_eq!(mc.compute(&vec![242,10,34,92]),     vec![159,220,88,157]);
        assert_eq!(mc.compute(&vec![198,198,198,198]),  vec![198,198,198,198]);
        assert_eq!(mc.compute(&vec![212,212,212,213]),  vec![213,213,215,214]);
        assert_eq!(mc.compute(&vec![45,38,49,76]),      vec![77,126,189,248]);
         
    }

    use crate::base::SolutionSet;

    #[test]
    pub fn it_enumerate_operator() {
        let _mc = MixColumnOperator::alone(255 ,4,
            vec![ // AES MC
                vec![ 2, 3, 1, 1],
                vec![ 1, 2, 3, 1],
                vec![ 1, 1, 2, 3],
                vec![ 3, 1, 1, 2],
                ]
        );

        let _to_bool = crate::base::BooleanAbstraction { };

        // TODO: à vérifier (autotest)
        let _result : SolutionSet = [ 
                (vec![1, 1, 1, 1, 1, 1, 0, 0], 64005      ),
                (vec![1, 1, 0, 1, 0, 1, 0, 1], 255        ),
                (vec![0, 0, 0, 0, 0, 0, 0, 0], 1          ),
                (vec![1, 1, 0, 1, 0, 1, 1, 0], 255        ),
                (vec![1, 1, 0, 1, 1, 0, 1, 1], 64005      ),
                (vec![1, 1, 0, 1, 1, 1, 1, 0], 64005      ),
                (vec![1, 1, 1, 1, 0, 0, 1, 1], 64005      ),
                (vec![1, 0, 1, 1, 1, 1, 0, 1], 64005      ),
                (vec![1, 1, 1, 0, 0, 1, 0, 1], 255        ),
                (vec![0, 1, 1, 1, 1, 1, 1, 1], 16323825   ),
                (vec![1, 0, 1, 1, 1, 1, 1, 0], 64005      ),
                (vec![1, 0, 1, 1, 0, 1, 1, 0], 255        ),
                (vec![1, 1, 0, 0, 1, 1, 0, 1], 255        ),
                (vec![1, 1, 1, 0, 1, 1, 1, 1], 16323825   ),
                (vec![0, 1, 1, 0, 1, 0, 1, 1], 255        ),
                (vec![1, 1, 0, 1, 1, 1, 0, 0], 255        ),
                (vec![1, 1, 1, 1, 0, 0, 0, 1], 255        ),
                (vec![1, 1, 0, 1, 1, 0, 0, 1], 255        ),
                (vec![0, 1, 1, 1, 1, 0, 1, 0], 255        ),
                (vec![1, 1, 1, 0, 1, 0, 1, 1], 64005      ),
                (vec![1, 1, 0, 0, 1, 0, 1, 1], 255        ),
                (vec![1, 1, 0, 1, 1, 1, 1, 1], 16323825   ),
                (vec![1, 0, 1, 0, 1, 0, 1, 1], 255        ),
                (vec![1, 0, 0, 0, 1, 1, 1, 1], 255        ),
                (vec![1, 0, 1, 1, 1, 0, 0, 1], 255        ),
                (vec![0, 0, 1, 1, 1, 1, 1, 1], 64005      ),
                (vec![1, 1, 1, 0, 0, 1, 1, 0], 255        ),
                (vec![1, 0, 1, 1, 0, 1, 1, 1], 64005      ),
                (vec![1, 0, 1, 1, 0, 0, 1, 1], 255        ),
                (vec![1, 1, 1, 0, 1, 1, 1, 0], 64005      ),
                (vec![1, 0, 0, 1, 0, 1, 1, 1], 255        ),
                (vec![1, 1, 0, 1, 1, 1, 0, 1], 64005      ),
                (vec![1, 1, 1, 0, 1, 0, 0, 1], 255        ),
                (vec![0, 1, 1, 1, 1, 0, 1, 1], 64005      ),
                (vec![1, 1, 1, 0, 1, 0, 1, 0], 255        ),
                (vec![1, 0, 0, 1, 1, 1, 1, 0], 255        ),
                (vec![0, 1, 0, 1, 0, 1, 1, 1], 255        ),
                (vec![0, 0, 1, 1, 1, 0, 1, 1], 255        ),
                (vec![0, 0, 1, 1, 1, 1, 1, 0], 255        ),
                (vec![1, 1, 0, 0, 0, 1, 1, 1], 255        ),
                (vec![1, 1, 1, 0, 0, 1, 1, 1], 64005      ),
                (vec![1, 0, 1, 1, 1, 0, 1, 0], 255        ),
                (vec![0, 1, 1, 1, 1, 0, 0, 1], 255        ),
                (vec![1, 0, 0, 1, 1, 1, 1, 1], 64005      ),
                (vec![1, 0, 0, 1, 1, 0, 1, 1], 255        ),
                (vec![0, 1, 1, 0, 0, 1, 1, 1], 255        ),
                (vec![0, 1, 0, 1, 1, 1, 1, 0], 255        ),
                (vec![1, 0, 1, 0, 0, 1, 1, 1], 255        ),
                (vec![0, 1, 1, 1, 1, 1, 0, 0], 255        ),
                (vec![1, 1, 1, 1, 1, 0, 1, 1], 16323825   ),
                (vec![0, 1, 1, 1, 0, 0, 1, 1], 255        ),
                (vec![1, 0, 1, 1, 1, 0, 1, 1], 64005      ),
                (vec![1, 1, 1, 0, 0, 0, 1, 1], 255        ),
                (vec![1, 0, 0, 1, 1, 1, 0, 1], 255        ),
                (vec![1, 1, 1, 1, 1, 0, 0, 1], 64005      ),
                (vec![0, 1, 1, 1, 0, 1, 1, 1], 64005      ),
                (vec![1, 0, 1, 0, 1, 1, 1, 0], 255        ),
                (vec![0, 0, 1, 0, 1, 1, 1, 1], 255        ),
                (vec![0, 1, 0, 1, 1, 1, 1, 1], 64005      ),
                (vec![0, 1, 0, 1, 1, 0, 1, 1], 255        ),
                (vec![0, 1, 1, 1, 1, 1, 0, 1], 64005      ),
                (vec![1, 1, 1, 1, 1, 1, 1, 1], 4162570275 ),
                (vec![1, 1, 1, 1, 1, 0, 1, 0], 64005      ),
                (vec![1, 0, 1, 1, 0, 1, 0, 1], 255        ),
                (vec![1, 1, 0, 0, 1, 1, 1, 1], 64005      ),
                (vec![0, 0, 0, 1, 1, 1, 1, 1], 255        ),
                (vec![0, 0, 1, 1, 1, 1, 0, 1], 255        ),
                (vec![1, 1, 1, 1, 0, 1, 1, 0], 64005      ),
                (vec![1, 1, 1, 0, 1, 1, 0, 1], 64005      ),
                (vec![0, 1, 1, 0, 1, 1, 1, 0], 255        ),
                (vec![1, 1, 0, 1, 0, 0, 1, 1], 255        ),
                (vec![1, 1, 1, 1, 0, 1, 0, 0], 255        ),
                (vec![0, 1, 0, 0, 1, 1, 1, 1], 255        ),
                (vec![0, 1, 0, 1, 1, 1, 0, 1], 255        ),
                (vec![1, 1, 1, 1, 1, 1, 0, 1], 16323825   ),
                (vec![1, 1, 1, 1, 1, 0, 0, 0], 255        ),
                (vec![0, 1, 1, 0, 1, 1, 0, 1], 255        ),
                (vec![0, 1, 1, 1, 0, 1, 1, 0], 255        ),
                (vec![1, 0, 1, 0, 1, 1, 1, 1], 64005      ),
                (vec![1, 1, 1, 1, 0, 1, 1, 1], 16323825   ),
                (vec![1, 1, 1, 1, 1, 1, 1, 0], 16323825   ),
                (vec![1, 1, 0, 1, 0, 1, 1, 1], 64005      ),
                (vec![1, 1, 0, 1, 1, 0, 1, 0], 255        ),
                (vec![1, 0, 1, 1, 1, 1, 0, 0], 255        ),
                (vec![1, 1, 0, 0, 1, 1, 1, 0], 255        ),
                (vec![0, 1, 1, 1, 1, 1, 1, 0], 64005      ),
                (vec![1, 1, 1, 1, 0, 0, 1, 0], 255        ),
                (vec![1, 0, 1, 1, 1, 1, 1, 1], 16323825   ),
                (vec![1, 1, 1, 0, 1, 1, 0, 0], 255        ),
                (vec![0, 1, 1, 1, 0, 1, 0, 1], 255        ),
                (vec![0, 0, 1, 1, 0, 1, 1, 1], 255        ),
                (vec![0, 1, 1, 0, 1, 1, 1, 1], 64005      ),
                (vec![1, 0, 1, 0, 1, 1, 0, 1], 255        ),
                (vec![1, 1, 1, 1, 0, 1, 0, 1], 64005      ),
                ].iter().cloned().collect::<SolutionSet>();

                #[cfg(debug_assertions)]
                debug_assert!(false);

                #[cfg(not(debug_assertions))]
                assert_eq! (
                    crate::base::enumerate(&_mc, &_to_bool), 
                    _result
                );
        
    }

}