use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct ByteToNibbleOperator { 
    base: BaseOperator,    
}

impl ByteToNibbleOperator {
    pub fn alone(i_size: Value, i_number: usize ) -> Self {
        Self { 
            base: BaseOperator::alone (i_size, i_number, 15, 2*i_number  )
        } 
    }
}

impl Operator for ByteToNibbleOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("ByteToNibble")
    }
}

impl Computable for ByteToNibbleOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {      
        input.iter().map(|i| 
            vec! [ (i >> 4) & 0xf, i & 0xf ]
        ).flatten().collect::<Vec<Value>>()
    }       
}
    

#[cfg(test)]
mod tests {
        
    use super::*;

    #[test]
    fn it_computes_operator() {
        let b2n = ByteToNibbleOperator::alone(255 ,1);                
    
        assert_eq!(b2n.compute( &vec![  0x0 ] ),vec![ 0x0, 0x0 ] );
        assert_eq!(b2n.compute( &vec![  0x12 ] ),vec![ 0x1, 0x2 ] );
        assert_eq!(b2n.compute( &vec![  0xfa ] ),vec![ 0xf, 0xa ] );
        assert_eq!(b2n.compute( &vec![  0xff ] ),vec![ 0xf, 0xf ] );        
    }

    use crate::base::SolutionSet;
    #[test]
    pub fn it_enumerate_operator() {     
        let b2n = ByteToNibbleOperator::alone(255 ,1 );
        let to_bool = crate::base::BooleanAbstraction { };

        let result : SolutionSet = [ 
                 (vec![0, 0, 0],   1), 
                 (vec![1, 0, 1],  15), 
                 (vec![1, 1, 0],  15), 
                 (vec![1, 1, 1], 225), 
        ].iter().cloned().collect::<SolutionSet>();
 
        assert_eq! (
            crate::base::enumerate(&b2n, &to_bool), 
            result
        );
    }

}