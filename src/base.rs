pub type Value = u32;

/// Reduce the domain size of variable
pub trait Abstraction {
    fn abstract_value(&self, value: Value) -> Value;
    fn max_value(&self) -> Value;
    fn name(&self) -> String;
    fn uid(&self) -> String;
}

// Question: Ai-je besoin de décorer un type?
/// Int (any size) -> bool
pub struct BooleanAbstraction {}

impl Abstraction for BooleanAbstraction {
    fn abstract_value(&self, value: Value) -> Value {
        if value == 0 {
            0
        } else {
            1
        }
    }

    fn max_value(&self) -> Value {
        return 1;
    }

    fn name(&self) -> String {
        String::from("BooleanA")
    }
    fn uid(&self) -> String {
        self.name()
    }
}

/// Initial domain for the variables
pub type Domain = std::ops::Range<Value>;

trait DomainTrait {
    fn size(&self) -> usize;
}
struct RangeDomain {
    r: std::ops::Range<Value>,
}

impl DomainTrait for RangeDomain {
    fn size(&self) -> usize {
        (self.r.end - self.r.start) as usize
    }
}

/// Generate a Domain (Range)
/// # Arguments
/// * `bits` - The number of bits of the domain
fn domain(bits: u64) -> Domain {
    0..(1 << bits)
}

// fn get_trans<'a>(&'a self) -> &'a HashMap<char, char>;

/// Trait to compute the value for an operator
pub trait Computable {
    fn compute(&self, input: &Vec<Value>) -> Vec<Value>;     
}

/// Operator vith input and output nodes:write!
pub struct BaseOperator {
    inputs:  Vec<Domain>,
    outputs: Vec<Domain>,
}

impl BaseOperator {
    pub fn inputs(&self) -> &Vec<Domain> {
        &self.inputs
    }
    pub fn outputs(&self) -> &Vec<Domain> {
        &self.outputs
    }

    /// Build an operator with disconnected inputs and outputsc
    /// # Arguments
    /// - i_size : the maximum value for input variables
    /// - i_number : the number of input variables
    /// - o_size : the maximum value for output variables
    /// - o_number : the number of output variables
    pub fn alone(i_size: Value, i_number: usize, o_size: Value, o_number: usize) -> Self {
        Self {
            inputs:  vec![0..(i_size + 1); i_number],
            outputs: vec![0..(o_size + 1); o_number],
        }
    }
}

use crate::json_save::hash_text;

pub trait Operator: Computable  {
    fn base(&self) -> &BaseOperator;
    fn check_compute(&self, input: &Vec<Value>) -> Vec<Value> {
        self.check_input(&input, self.base().inputs());
        let output = self.compute(input);
        self.check_input(&output, self.base().outputs());
        output        
    }
    fn check_input(&self, values: &Vec<Value>, domains: &Vec<Domain>) {
        assert_eq!(values.len(), domains.len());

        for i in 0..values.len() {
            if ! domains[i].contains( & values[i] ) {
                panic!("Error: {} not in {:?} !!!", &values[i], &domains[i]);
            }
        }
    }        

    /// Unique name with hash if too long for a filename
    fn uid_short(&self) -> String {
       let parameters = self.parameters2s();
       let parameters = if parameters == "" { 
           parameters
        } else { 
            hash_text(parameters.as_str() ) 
        };
        
       format!("{}{}{}{}", self.name(), parameters,self.inputs2s(), self.outputs2s() )
    }
    
    /// Unique name
    fn uid(&self) -> String {
        format!("{}{}{}{}", self.name(), self.parameters2s(),self.inputs2s(), self.outputs2s() )
    }

    /// Short name
    fn name(&self) -> String;

    /// Dynamic part of the name
    fn parameters2s(&self) -> String {
       "".to_string()
    }
  
    fn inputs2s(&self) -> String {
        let mut buffer = String::from(".i");
        for domain in self.base().inputs() {
            let str= format!(".{}_{}", domain.start, domain.end-1);
            buffer.push_str(&str);
        }
        buffer   
    }

    fn outputs2s(&self) -> String {
        let mut buffer = String::from(".o");
        for domain in self.base().outputs() {
            let str= format!(".{}_{}", domain.start, domain.end-1);
            buffer.push_str(&str);
        }
        buffer   
    }
}

use fxhash::FxHashMap;

//pub type SolutionSet = HashMap<Vec<Value>, u64, >;
pub type SolutionSet = FxHashMap<Vec<Value>, u64>;

// Needed to enumerate the multiple cartesian product
use itertools::Itertools;

//fn enumerate( operator: &Operator, abstraction: &Abstraction  ) -> Vec<Value> /* set */{
/// Enumerate all the inputs, compute the solution, abstract the input and the solution, merge the solution with the already existing ones
/// # Params
/// - operator: the operation to perform
/// - abstraction: The abstraction to use
pub fn enumerate<Oper: Operator + Computable + ?Sized>(
    operator: &Oper,
    abstraction: &impl Abstraction,
) -> SolutionSet /* set */ {
    // create perfect hashmap with an array
    let nb_elts:usize = operator.base().inputs().len() + operator.base().outputs().len();
    let nb_entries:usize = (abstraction.max_value()+1).pow(nb_elts as u32) as usize;
    let mut table: Vec<u64> = vec![0; nb_entries];

    // let mut sol = SolutionSet::default();

    // https://ridiculousfish.com/blog/posts/least-favorite-rust-type.html

    let domains = operator
        .base()
        .inputs()
        .iter()
        .map(|r| r.clone().collect::<Vec<Value>>())
        .multi_cartesian_product();

    let mut abstracted: Vec<Value> =
        Vec::with_capacity(operator.base().inputs().len() + operator.base().outputs().len());
    // enumerate all the possible inputs
    for instance in domains {
        abstracted.clear();
        // compute input abstraction
        for &i in &instance {
            abstracted.push(abstraction.abstract_value(i));
        }

        /*
                let mut abstract_input = instance
                    .iter()
                    .map(|i| abstraction.abstract_value(*i))
                    .collect::<Vec<Value>>();
        */

        // Prend le premier car sortie multiple FixMe
        // compute output abstraction
        #[cfg(debug_assertions)]
        let computed = operator.check_compute(&instance);

        #[cfg(not(debug_assertions))]
        let computed = operator.compute(&instance);

        for i in computed {
            abstracted.push(abstraction.abstract_value(i));
        }
        // insert the abstract tuple inside the map
        let mut hash:usize = 0;
        for e in abstracted.iter() {
            hash *= (abstraction.max_value() as usize)+1;
            hash += *e as usize;
        }
        table[hash] += 1;

        /*
        let mut computed = operator
            .compute(&instance)
            .iter()
            .map(|i| abstraction.abstract_value(*i))
            .collect::<Vec<Value>>();
        */
        //abstract_input.append(&mut computed);

        /*
        sol.insert (
            abstract_input.clone(),
            if sol.contains_key(&abstract_input) {
                // 2 accès à la map via hash. Bof
                *sol.get(&abstract_input).unwrap()+1
            } else {
                1
            }
        );
        TODO: remove me
        */
        // TODO: réfléchir à ownership + mutabilité des clés
        // *sol.entry(abstracted.clone()).or_insert(0) += 1;

        /*
        match sol.entry(abstract_input.clone()) {
            Entry::Occupied(o) => {
                let info = o.into_mut();
                *info += 1;
            }
            Entry::Vacant(v) => {
                v.insert(1);
            }
        } // TODO: remove me
         */
    }
    // sol

    let mut res = SolutionSet::default();
    for i in 0..nb_entries {
        if table[i] > 0 {
            let mut tmp = i as Value;
            let k:Value = abstraction.max_value()+1;
            let mut abstracted:Vec<Value> = Vec::with_capacity(nb_elts);
            for _ in 0..nb_elts  {
                abstracted.push(
                    (tmp%k) as Value
                );
                tmp /= k;
            }
            abstracted.reverse();
            res.insert(abstracted, table[i]);
        }
    }

    return res;
}

const fn num_bits<T>() -> usize {
    std::mem::size_of::<T>() * 8
}
pub fn nb_necessary_bits(x: usize) -> u32 {
    // Pas de log_2 sur les entiers :(
    if x == 0 {
        1
    } else {
        num_bits::<usize>() as u32 - x.leading_zeros() - 1
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_builds_domains() {
        let d1: Domain = 0..256;
        let d2 = domain(8);

        assert_eq!(d1, d2);
    }

    #[test]
    fn it_computes_nb_necessary_bits() {
        assert_eq!(nb_necessary_bits(8), 3);
        assert_eq!(nb_necessary_bits(9), 3);
        assert_eq!(nb_necessary_bits(15), 3);
        assert_eq!(nb_necessary_bits(16), 4);
    }

    #[test]
    fn it_enumerates_domains() {
        let d1: Domain = 0..256;
        let mut accu = 0;

        for n in d1 {
            accu += n;
        }

        assert_eq!(accu, 32640);
    }
}
