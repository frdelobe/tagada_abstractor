use crate::base::{BaseOperator, Operator, Computable, Value};

pub struct BitsToNibbleOperator { 
    base: BaseOperator,    
}

impl BitsToNibbleOperator {
    pub fn alone() -> Self {
        Self { 
            base: BaseOperator::alone (1, 4, 15, 1  )
        } 
    }
}

impl Operator for BitsToNibbleOperator {
    fn base(&self)  -> &BaseOperator {
        &self.base
    }

    fn name(&self) -> String {
        String::from("BitToNibble")
    }
}

impl Computable for BitsToNibbleOperator {
    fn compute(&self, input : & Vec<Value>) -> Vec<Value> {
        let mut accu = 0;
        for bit in input {
            accu *= 2;

            if *bit == 1 {
                accu += 1;
            }
        }

        vec![ accu ] 
    }        
}
    

#[cfg(test)]
mod tests {
        
    use super::*;

    #[test]
    fn it_computes_operator() {
        let b2n = BitsToNibbleOperator::alone();                
    
        assert_eq!(b2n.compute( &vec![ 0, 0 ] ),vec![ 0 ] );
        assert_eq!(b2n.compute( &vec![ 0, 1 ] ),vec![ 1 ] );
        assert_eq!(b2n.compute( &vec![ 1, 0 ] ),vec![ 2 ] );
        assert_eq!(b2n.compute( &vec![ 1, 1 ] ),vec![ 3 ] );
    }

    use crate::base::SolutionSet;
    #[test]
    pub fn it_enumerate_operator() {        
        let b2n = BitsToNibbleOperator::alone();
        let to_bool = crate::base::BooleanAbstraction { };

        let result : SolutionSet = [ 
                 (vec![0, 0, 0, 0, 0 ], 1), 
                 (vec![0, 0, 0, 1, 1 ], 1),                 
                 (vec![0, 0, 1, 0, 1 ], 1),                 
                 (vec![0, 0, 1, 1, 1 ], 1),                  
                 (vec![0, 1, 0, 0, 1 ], 1), 
                 (vec![0, 1, 0, 1, 1 ], 1),                 
                 (vec![0, 1, 1, 0, 1 ], 1),                 
                 (vec![0, 1, 1, 1, 1 ], 1),                  
                 (vec![1, 0, 0, 0, 1 ], 1), 
                 (vec![1, 0, 0, 1, 1 ], 1),                 
                 (vec![1, 0, 1, 0, 1 ], 1),                 
                 (vec![1, 0, 1, 1, 1 ], 1),                  
                 (vec![1, 1, 0, 0, 1 ], 1), 
                 (vec![1, 1, 0, 1, 1 ], 1),                 
                 (vec![1, 1, 1, 0, 1 ], 1),                 
                 (vec![1, 1, 1, 1, 1 ], 1), 
        ].iter().cloned().collect::<SolutionSet>();
 
        assert_eq! (
            crate::base::enumerate(&b2n, &to_bool), 
            result
        );          
    }

}