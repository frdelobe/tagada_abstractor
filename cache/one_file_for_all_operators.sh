#!/bin/bash


DEST=all.json
first=true

echo "[" >> $DEST
for file in *.abstracted_oper.json; do
    if [ "$first" != 'true' ]; then
        first='false'
        echo ','
    fi
    cat $file
done >> $DEST
echo "]" >> $DEST
echo "All operators stored in $DEST"