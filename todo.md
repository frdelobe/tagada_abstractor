# Abstractor

TODO: Intégrer n_bit à domain
* Domain
    * Range: mini(), maxi(), iter(), len(), contains(Value)->bool 
    * *Manque*: nb_bits()

TODO: Sur la gestion des constantes: Quand value existe en JSON, il réduit le range de la variable

NbActiveBitsAbstraction
* nbActiveBits = Byte -> {0,1,2,3,4,5,6,7,8}
* uid: toujours défini automatiquement
  * sbox : Avec les valeurs de la table
  * MC: Pareil
* name: Par défaut le uid, sinon un custom

# Invocation d'abstractor

## Version ligne de commande

* abstractor xor 0..15 2  
  * [ ( [ 0 0 0 ], 1),  [ 0 1 1 ], 15), ..., [ 1 1 1 ], 15^2) ] // Logique
   
* abstractor mc 0..15 1 2 3 4 5 6


* serde_json => Json en rust

## Todo Const

TODO: Que faire des constantes? => PAsser une value dans la json. Au parse dans abstrator, réduire le domaine du range. 


## Refactor

TODO: Fusionner les JStructures avec les autres

## Bug

TODO: gérer les values

## Todos court terme

TODO: Appel exe abstractor depuis tagada
TODO: Mettre un sous module pour abstracteur
